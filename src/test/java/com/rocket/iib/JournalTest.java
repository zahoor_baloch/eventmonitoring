package com.rocket.iib;

import org.junit.Before;
import org.junit.Test;

import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.rockit.iib.Application;
import com.rockit.iib.controller.JournalController;
import com.rockit.iib.service.JournalService;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration

public class JournalTest {
	
	@Autowired
    private WebApplicationContext wac;
	
	private MockMvc mockMvc;
	
	@Autowired
	JournalController journalController;
	
	@Autowired
	JournalService journalService;
	
	@Before
	    public void setup() {
	        mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
	    }

	
	@Test
	public void findTest(){
		
	}
	
	@Test
	public void transactionTest() throws Exception{
		ResultMatcher ok = MockMvcResultMatchers.status().isOk();
		
		MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/transaction");
		
		mockMvc.perform(builder).andExpect(ok);
		
	}
}
