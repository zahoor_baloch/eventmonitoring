package com.rockit.iib.controller;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.rockit.iib.model.*;
import com.rockit.iib.service.JournalService;

@Controller
public class JournalController {

	@Autowired
	JournalService journalService;
		
	@RequestMapping("/find")
	public String find(@PageableDefault(page=0, size=4)  Pageable pageable, @RequestParam Map<String, String> params, Model model) {
		
		try {
			journalService.find(pageable, params, model);
		} catch (Exception e) {
			return "redirect:/find";
		}		
		return "journal";
	}
	
	@RequestMapping("/transaction")
	public String findTransaction(@PageableDefault(page=0, size=4)  Pageable pageable, @RequestParam Map<String, String> params, Model model) {
		try {
			
			journalService.findTransaction(pageable, params, model);
		} catch (Exception e) {
			//return "redirect:/";
		}	
		return "transaction";
		
	}
	
	@RequestMapping(value="/findData", produces ={"application/xml","text/xml" }, consumes = MediaType.ALL_VALUE )
	@ResponseBody
	public JournalData findData(@RequestParam Map<String, String> params, Model model) {
		
		try {
			return journalService.findData (params, model);
		} catch (Exception e) {
			return null;
		}		
	}
	
	
}
