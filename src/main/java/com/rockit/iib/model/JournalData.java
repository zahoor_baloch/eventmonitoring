package com.rockit.iib.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.xml.bind.annotation.*;

@XmlRootElement(name = "JournalData")
@XmlType(propOrder = {"id", "dataType", "encoding", "data"})

@Entity(name = "JournalData")
@Table(name = "WMB_BINARY_DATA")

public class JournalData {
	@Id
	@GeneratedValue
	@Column(name = "WMB_MSGKEY", nullable = false)
	private String id;

	@Column(name = "DATA_TYPE")
	private int dataType;
	
	@Column(name = "ENCODING")
	private String encoding;
	
	@Lob
	@Column(name = "DATA")
	private String data;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getDataType() {
		return dataType;
	}

	public void setDataType(int dataType) {
		this.dataType = dataType;
	}

	public String getEncoding() {
		return encoding;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}
}
