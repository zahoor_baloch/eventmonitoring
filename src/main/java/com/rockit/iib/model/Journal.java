package com.rockit.iib.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "Journal")
@Table(name = "WMB_MSGS")
public class Journal {

	@Id
	@GeneratedValue
	@Column(name = "WMB_MSGKEY", nullable = false)
	private String id;

	@Column(name = "HAS_BITSTREAM")
	private char hasBitStream;
	
	@Column(name = "HAS_EXCEPTION")
	private char hasException;
	
	@Column(name = "HAS_USERDATA")
	private char hasUserUpdate;
	
	@Column(name = "EVENT_TYPE")
	private String eventType;
	
	@Column(name = "EVENT_NAME")
	private String eventName;
	
	@Column(name = "EVENT_SRCADDR")
	private String eventSourceAddress;
	
	@Column(name = "BROKER_NAME")
	private String brokerName;
	
	@Column(name = "BROKER_UUID")
	private String brokerId;
	
	@Column(name = "EXGRP_NAME")
	private String egName;
	
	@Column(name = "EXGRP_UUID")
	private String edId;
	
	@Column(name = "MSGFLOW_NAME")
	private String flowName;
	
	@Column(name = "MSGFLOW_UUID")
	private String flowId;
	
	@Column(name = "APPL_UUID")
	private String applicationId;
	
	@Column(name = "APPL_NAME")
	private String applicationName;
	
	@Column(name = "LIBRARY_UUID")
	private String libraryId;
	
	@Column(name = "LIBRARY_NAME")
	private String libraryName;
	
	@Column(name = "NODE_NAME")
	private String nodeName;
	
	@Column(name = "NODE_TYPE")
	private String nodeType;
	
	@Column(name = "DETAIL")
	private String detail;
	
	@Column(name = "TERMINAL_NAME")
	private String terminalName;
	
	@Column(name = "KEY_FLD_1_NM")
	private String field1Key;	
	
	@Column(name = "KEY_FLD_2_NM")
	private String field2Key;

	@Column(name = "KEY_FLD_3_NM")
	private String field3Key;
	
	@Column(name = "KEY_FLD_4_NM")
	private String field4Key;
	
	@Column(name = "KEY_FLD_5_NM")
	private String field5Key; 
	
	@Column(name = "EVENT_TIMESTAMP")
	private String eventTimeStamp;

	@Column(name = "LOCAL_TRANSACTION_ID")
	private String localTransactionId;
	
	@Column(name = "GLOBAL_TRANSACTION_ID")
	private String globalTransactionId;
	
	@Column(name = "PARENT_TRANSACTION_ID")
	private String parentTransactionId;
	

	public void setId(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}
	
	public char getHasBitStream() {
		return hasBitStream;
	}

	public void setHasBitStream(char hasBitstream) {
		this.hasBitStream = hasBitstream;
	}

	public char getHasException() {
		return hasException;
	}

	public void setHasException(char hasException) {
		this.hasException = hasException;
	}

	public char getHasUserUpdate() {
		return hasUserUpdate;
	}

	public void setHasUserUpdate(char hasUserUpdate) {
		this.hasUserUpdate = hasUserUpdate;
	}

	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getEventSourceAddress() {
		return eventSourceAddress;
	}

	public void setEventSourceAddress(String eventSourceAddress) {
		this.eventSourceAddress = eventSourceAddress;
	}

	public String getBrokerName() {
		return brokerName;
	}

	public void setBrokerName(String brokerName) {
		this.brokerName = brokerName;
	}

	public String getBrokerId() {
		return brokerId;
	}

	public void setBrokerId(String brokerId) {
		this.brokerId = brokerId;
	}

	public String getEgName() {
		return egName;
	}

	public void setEgName(String egName) {
		this.egName = egName;
	}

	public String getEdId() {
		return edId;
	}

	public void setEdId(String edId) {
		this.edId = edId;
	}

	public String getFlowName() {
		return flowName;
	}

	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}

	public String getFlowId() {
		return flowId;
	}

	public void setFlowId(String flowId) {
		this.flowId = flowId;
	}

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public String getLibraryId() {
		return libraryId;
	}

	public void setLibraryId(String libraryId) {
		this.libraryId = libraryId;
	}

	public String getLibraryName() {
		return libraryName;
	}

	public void setLibraryName(String libraryName) {
		this.libraryName = libraryName;
	}

	public String getNodeName() {
		return nodeName;
	}

	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}

	public String getNodeType() {
		return nodeType;
	}

	public void setNodeType(String nodeType) {
		this.nodeType = nodeType;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getTerminalName() {
		return terminalName;
	}

	public void setTerminalName(String terminalName) {
		this.terminalName = terminalName;
	}

	public String getField1Key() {
		return field1Key;
	}

	public void setField1Key(String field1Key) {
		this.field1Key = field1Key;
	}

	public String getField2Key() {
		return field2Key;
	}

	public void setField2Key(String field2Key) {
		this.field2Key = field2Key;
	}

	public String getField3Key() {
		return field3Key;
	}

	public void setField3Key(String field3Key) {
		this.field3Key = field3Key;
	}

	public String getField4Key() {
		return field4Key;
	}

	public void setField4Key(String field4Key) {
		this.field4Key = field4Key;
	}

	public String getField5Key() {
		return field5Key;
	}

	public void setField5Key(String field5Key) {
		this.field5Key = field5Key;
	}

	public String getEventTimeStamp() {
		return eventTimeStamp;
	}

	public void setEventTimeStamp(String eventTimestamp) {
		this.eventTimeStamp = eventTimestamp;
	}

	public String getLocalTransactionId() {
		return localTransactionId;
	}

	public void setLocalTransactionId(String localTransactionId) {
		this.localTransactionId = localTransactionId;
	}

	public String getGlobalTransactionId() {
		return globalTransactionId;
	}

	public void setGlobalTransactionId(String globalTransactionId) {
		this.globalTransactionId = globalTransactionId;
	}

	public String getParentTransactionId() {
		return parentTransactionId;
	}

	public void setParentTransactionId(String parentTransactionId) {
		this.parentTransactionId = parentTransactionId;
	}

	@Override
	public String toString() {
		return "IIBMessage [id=" + id + ", hasBitstream=" + hasBitStream + ", hasException="
				+ hasException + ", hasUserUpdate=" + hasUserUpdate + ", eventType=" + eventType + ", eventName="
				+ eventName + ", eventSourceAddress=" + eventSourceAddress + ", brokerName=" + brokerName
				+ ", brokerId=" + brokerId + ", egName=" + egName + ", edId=" + edId + ", flowName=" + flowName
				+ ", flowId=" + flowId + ", applicationId=" + applicationId + ", applicationName=" + applicationName
				+ ", LibraryUUID=" + libraryId + ", libraryName=" + libraryName + ", nodeName=" + nodeName
				+ ", nodeType=" + nodeType + ", detail=" + detail + ", terminalName=" + terminalName + ", field1Key="
				+ field1Key + ", field2Key=" + field2Key + ", field3Key=" + field3Key + ", field4Key=" + field4Key
				+ ", field5Key=" + field5Key + ", eventTimestamp=" + eventTimeStamp + ", localTransactionId="
				+ localTransactionId + ", globalTransactionId=" + globalTransactionId + ", parentTransactionId="
				+ parentTransactionId + "]";
	}
	
	
	
	
	
}
