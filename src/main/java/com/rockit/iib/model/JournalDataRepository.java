package com.rockit.iib.model;

import org.springframework.data.repository.CrudRepository;

public interface JournalDataRepository extends CrudRepository<JournalData, Integer>{
	
	JournalData findById(String id);
	
}