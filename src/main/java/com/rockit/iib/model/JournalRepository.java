package com.rockit.iib.model;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

import javax.transaction.Transactional;


@Transactional
public interface JournalRepository  extends PagingAndSortingRepository<Journal, Integer>{
	
	@Query("SELECT j FROM Journal j WHERE j.eventTimeStamp BETWEEN ?1 AND ?2")
	public Page<Journal> select(String from, String to, Pageable pageable) ;
	
	//SELECT DISTINCT  LOCAL_TRANSACTION_ID FROM ROCKIT.WMB_MSGS  WHERE EVENT_TIMESTAMP BETWEEN '2016-10-13 13:39:05.272' AND '2016-10-13 13:39:05.276'

	@Query("SELECT DISTINCT j FROM Journal j")
	public Page<Journal> selectDistinctTId(Pageable pageable);
}
