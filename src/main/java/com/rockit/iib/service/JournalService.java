package com.rockit.iib.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import com.rockit.iib.model.*;
import com.rockit.iib.wrapper.PageWrapper;

@Service
public class JournalService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private JournalRepository journalRepository;
	
	@Autowired
	private JournalDataRepository journalDataRepository;
	
	public void find (Pageable pageable, Map<String, String> params, Model model) throws Exception{
	
		Page<Journal> entries = null;
		String from = params.get("from"), to = params.get("to");

		if((from != null && !from.equals("")) && (to !=null && !from.equals(""))){
			
			Date fromDate = null, toDate = null;
			
			DateFormat parser = new SimpleDateFormat("MM/dd/yyyy h:mm a", Locale.ENGLISH),
			fromatter = new SimpleDateFormat("yyyy-MM-dd HH.mm.ss.SSS"); 
		
				fromDate = parser.parse(from);
				toDate = parser.parse(to);
				
				entries =  journalRepository.select(fromatter.format(fromDate), fromatter.format(toDate), pageable);
				
				model.addAttribute("to", to);
				model.addAttribute("from", from);
					
		}else {
			entries = journalRepository.findAll(pageable);
		}
		
		PageWrapper<Journal> page = new PageWrapper<Journal>(entries, "/find");
		
		model.addAttribute("page", page); 
		model.addAttribute("entries", entries);
		
	}

	public void findTransaction(Pageable pageable, Map<String, String> params, Model model) {
		// TODO Auto-generated method stub
		logger.info("findTransaction.............................");
		Page<Journal> entries =  journalRepository.selectDistinctTId(pageable);
		//Page<Journal> entries =  journalRepository.findAll(pageable);
		if(entries == null){
			logger.info("entries is nullllllllllllllllllllllllllll");
		}else{
			logger.info("entries SIZE::::::::::::::" + entries.getSize());
		}
		
		PageWrapper<Journal> page = new PageWrapper<Journal>(entries, "/transaction");
		logger.info("" + page.getSize());
		model.addAttribute("page", page); 
		model.addAttribute("entries", entries);
	}
	
	public JournalData findData(Map<String, String> params, Model model) {
		return journalDataRepository.findById(params.get("id"));
	}

	
}
